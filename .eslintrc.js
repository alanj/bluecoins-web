module.exports = {
  overrides: [
    {
      // Svelte TS files
      files: ['**/*.svelte'],
      parser: '@typescript-eslint/parser',
      extends: [
        'eslint:recommended',
        'airbnb-base',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
      ],
      parserOptions: { // add these parser options
        tsconfigRootDir: __dirname,
        project: './tsconfig.json',
        extraFileExtensions: ['.svelte'],
        ecmaVersion: 2019,
        sourceType: 'module',
      },
      env: {
        es6: true,
        browser: true,
      },
      plugins: [
        'svelte3',
        '@typescript-eslint', // add the TypeScript plugin
      ],
      overrides: [
        {
          files: ['./src/**/*.svelte'],
          processor: 'svelte3/svelte3',
        },
      ],
      rules: {
        'import/no-mutable-exports': 0, // https://github.com/sveltejs/eslint-plugin-svelte3/blob/master/OTHER_PLUGINS.md
        'import/prefer-default-export': 0,
        'no-shadow': 'off', // Added because enums were throwing this error
        '@typescript-eslint/no-shadow': ['error'],
        // -- the below import rules are added because .ts files
        // could not be imported without an error.
        // Can be possibly removed or at least keep the error for some type of files.
        'import/no-unresolved': 0,
        'import/extensions': ['error', 'never'],
        // --
      },
      settings: {
        'svelte3/typescript': true, // load TypeScript as peer dependency
      },
    },
    {
      files: ['**/*.ts'],
      parser: '@typescript-eslint/parser',
      plugins: [
        '@typescript-eslint',
      ],
      extends: [
        'eslint:recommended',
        'airbnb-base',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
      ],
    },
    {
      // For tests
      files: [
        '**/*.spec.js',
      ],
      env: { jest: true },
      // For svelte-mock library
      globals: { svelteMock: 'readonly' },
      rules: {
        'import/no-unresolved': 0,
        'import/first': 0,
      },
    },
    {
      // For config files
      files: [
        './*.js',
      ],
      rules: {
        'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
        'import/first': 0,
      },
    },
  ],
  // Other js files
  extends: [
    'eslint:recommended',
    'airbnb-base',
  ],
  ignorePatterns: ['**/*.css', 'node_modules/*', 'dist/*'],
};
