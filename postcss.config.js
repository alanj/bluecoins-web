const PostCssImport = require('postcss-import');
const TailwindCss = require('tailwindcss');
const AutoPrefixer = require('autoprefixer');

module.exports = {
  plugins: [
    PostCssImport,
    TailwindCss,
    AutoPrefixer,
  ],
};
