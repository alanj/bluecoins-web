import { render } from '@testing-library/svelte';

jest.mock('@svelte/DragAndDrop.svelte');
import DragAndDrop from '@svelte/DragAndDrop.svelte';
import App from '../App.svelte';

svelteMock.mockImplementation(DragAndDrop);

it('should render', () => {
  render(App);

  expect(DragAndDrop).toHaveInstanceWithProps(['processFile']);
});
