import { render, fireEvent } from '@testing-library/svelte';

import Button from '../svelte/Button.svelte';

it('should render', () => {
  const onClickMock = jest.fn();
  const results = render(Button, { content: 'TEST_CONTENT', onClick: onClickMock });

  expect(() => results.getByText('TEST_CONTENT')).not.toThrow();
});

it('should handle click events', () => {
  const onClickMock = jest.fn();
  const results = render(Button, { content: 'TEST_CONTENT', onClick: onClickMock });
  const button = results.getByText('TEST_CONTENT');

  fireEvent.click(button);

  expect(onClickMock).toBeCalled();
});
