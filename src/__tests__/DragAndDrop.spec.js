import { render, fireEvent } from '@testing-library/svelte';

import DragAndDrop from '../svelte/DragAndDrop.svelte';

jest.mock('../svelte/FileInput');
import FileInput from '../svelte/FileInput.svelte';

svelteMock.mockImplementation(FileInput);

it('should render', () => {
  const processFileMock = jest.fn();
  const results = render(DragAndDrop, { processFile: processFileMock });
  const htmlElement = results.container;

  expect(htmlElement.getElementsByTagName('section').length).toBe(1);
  expect(htmlElement.getElementsByTagName('p')[0].innerHTML).toBe('Drag and drop or upload filename.fydb file here');
  expect(FileInput).toHaveInstanceWithProps({ changeCallback: processFileMock });
});

it('should change drag-and-drop section border color on dragEnter and drop', async () => {
  const processFileMock = jest.fn();
  const results = render(DragAndDrop, { processFile: processFileMock });
  const htmlElement = results.container;
  const sectionElSelector = htmlElement.getElementsByTagName('section')[0];

  expect(sectionElSelector.classList.contains('border-gray-500')).toBe(true);
  expect(sectionElSelector.classList.contains('border-blue-500')).toBe(false);

  await fireEvent.dragEnter(sectionElSelector);

  expect(sectionElSelector.classList.contains('border-gray-500')).toBe(false);
  expect(sectionElSelector.classList.contains('border-blue-500')).toBe(true);

  await fireEvent.drop(sectionElSelector);

  expect(sectionElSelector.classList.contains('border-gray-500')).toBe(true);
  expect(sectionElSelector.classList.contains('border-blue-500')).toBe(false);
});

it('should execute callback on drop event', async () => {
  const processFileMock = jest.fn();
  const results = render(DragAndDrop, { processFile: processFileMock });
  const htmlElement = results.container;
  const sectionElSelector = htmlElement.getElementsByTagName('section')[0];
  const fileMock = jest.fn();
  const dropEventMock = {
    dataTransfer: {
      files: [fileMock],
    },
  };

  await fireEvent.drop(sectionElSelector, dropEventMock);

  expect(processFileMock).toBeCalledTimes(1);
  expect(processFileMock).toBeCalledWith(fileMock);
});
