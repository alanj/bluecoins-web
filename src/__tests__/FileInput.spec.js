import { render, fireEvent } from '@testing-library/svelte';

import FileInput from '../svelte/FileInput.svelte';

it('should render', () => {
  const results = render(FileInput, { changeCallback: jest.fn() });
  const inputElement = results.container.getElementsByTagName('input')[0];
  const buttonElement = results.getByText('Upload File');

  expect(inputElement.id).toBe('fileInput');
  expect(inputElement.type).toBe('file');
  expect(inputElement.accept).toBe('.fydb');
  expect(() => buttonElement).not.toThrow();
});

it('should execute file input click on button click', () => {
  const onFileInputClickMock = jest.fn();
  const results = render(FileInput, { changeCallback: jest.fn() });
  const buttonElement = results.getByText('Upload File');
  const inputElement = results.container.getElementsByTagName('input')[0];
  inputElement.addEventListener('click', onFileInputClickMock);

  fireEvent.click(buttonElement);

  expect(onFileInputClickMock).toBeCalled();
});

it('should execute callback on file input change', () => {
  const changeCallbackMock = jest.fn();
  const results = render(FileInput, { changeCallback: changeCallbackMock });
  const inputElement = results.container.getElementsByTagName('input')[0];
  const fileMock = jest.fn();
  const mockFileChangeEventAttributes = {
    target: {
      files: {
        item: jest.fn((index) => (index === 0 ? fileMock : undefined)),
        length: 1,
      },
    },
  };

  fireEvent.change(inputElement, mockFileChangeEventAttributes);

  expect(changeCallbackMock).toBeCalledTimes(1);
  expect(changeCallbackMock).toBeCalledWith(fileMock);
});
