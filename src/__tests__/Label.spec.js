import { render } from '@testing-library/svelte';

import Label from '../svelte/Label.svelte';

it('should render', () => {
  const results = render(Label, { content: 'TEST_CONTENT' });

  expect(results.container.getElementsByTagName('span').length).toEqual(1);
  expect(() => results.getByText('TEST_CONTENT')).not.toThrow();
});

it('should not render if empty content', () => {
  const results = render(Label, { content: '' });

  expect(results.container.getElementsByTagName('span').length).toEqual(0);
});

it('should not render if content is null', () => {
  const results = render(Label, { content: null });

  expect(results.container.getElementsByTagName('span').length).toEqual(0);
});
