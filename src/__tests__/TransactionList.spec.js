import { render } from '@testing-library/svelte';

import TransactionList from '../svelte/TransactionList.svelte';

jest.mock('../svelte/TransactionRow');
import TransactionRow from '../svelte/TransactionRow.svelte';

svelteMock.mockImplementation(TransactionRow);

const mockTransactions = [
  { mockKey: 'TEST_TRANSACTION_1' },
  { mockKey: 'TEST_TRANSACTION_2' },
  { mockKey: 'TEST_TRANSACTION_3' },
];

it('should render', () => {
  const results = render(TransactionList, { transactions: mockTransactions });

  expect(results.container.getElementsByTagName('li').length).toEqual(3);
  expect(TransactionRow).toHaveInstanceWithProps({ transaction: { mockKey: 'TEST_TRANSACTION_1' } });
  expect(TransactionRow).toHaveInstanceWithProps({ transaction: { mockKey: 'TEST_TRANSACTION_2' } });
  expect(TransactionRow).toHaveInstanceWithProps({ transaction: { mockKey: 'TEST_TRANSACTION_3' } });
});
