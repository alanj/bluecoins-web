import { render } from '@testing-library/svelte';

import TransactionRow from '../svelte/TransactionRow.svelte';

jest.mock('../svelte/Label');
import Label from '../svelte/Label.svelte';

svelteMock.mockImplementation(Label);

const expenseIconBgClass = 'bg-red-500';
const incomeIconBgClass = 'bg-green-500';
const transferIconBgClass = 'bg-blue-500';
const defaultIconBgClass = 'bg-gray-500';
const expenseTextClass = 'text-red-500';
const incomeTextClass = 'text-green-500';
const defaultTextClass = 'text-gray-500';
const mockTransaction = {
  itemName: 'TEST_ITEM_NAME',
  accountBalance: 12345,
  accountName: 'TEST_AC_NAME',
  amount: 98,
  transactionTypeName: 'TEST_TRANSACTION_TYPE',
  childCategoryName: 'TEST_CHILD_CATEGORY_NAME',
  parentCategoryName: 'TEST_PARENT_CATEGORY_NAME',
  labelName: 'TEST_LABEL_NAME',
  notes: 'TEST_NOTES',
  transactionCurrency: 'USD',
  time: 'TEST_TIME',
  date: 'TEST_DATE',
};

it('should render', () => {
  const results = render(TransactionRow, { transaction: mockTransaction });

  expect(results.container.getElementsByTagName('i').length).toEqual(1);
  expect(() => results.getByText('TEST_ITEM_NAME')).not.toThrow();
  expect(() => results.getByText('TEST_CHILD_CATEGORY_NAME')).not.toThrow();
  expect(() => results.getByText('TEST_NOTES')).not.toThrow();
  expect(Label).toHaveInstanceWithProps({ content: 'TEST_LABEL_NAME' });
  expect(() => results.getByText('$98.00')).not.toThrow();
  expect(() => results.getByText('TEST_AC_NAME | $12,345.00')).not.toThrow();
});

it('should show transfer icon background color when transaction is a transfer', () => {
  const results = render(TransactionRow, { transaction: { ...mockTransaction, transactionTypeName: 'Transfer' } });
  const iconElements = results.container.getElementsByTagName('i');

  expect(iconElements.length).toEqual(1);
  expect(iconElements[0].classList.contains(expenseIconBgClass)).toBeFalsy();
  expect(iconElements[0].classList.contains(incomeIconBgClass)).toBeFalsy();
  expect(iconElements[0].classList.contains(transferIconBgClass)).toBeTruthy();
  expect(iconElements[0].classList.contains(defaultIconBgClass)).toBeFalsy();
});

it('should show default icon background color if new account transaction', () => {
  const results = render(TransactionRow, { transaction: { ...mockTransaction, transactionTypeName: 'New Account' } });
  const iconElements = results.container.getElementsByTagName('i');

  expect(iconElements.length).toEqual(1);
  expect(iconElements[0].classList.contains(expenseIconBgClass)).toBeFalsy();
  expect(iconElements[0].classList.contains(incomeIconBgClass)).toBeFalsy();
  expect(iconElements[0].classList.contains(transferIconBgClass)).toBeFalsy();
  expect(iconElements[0].classList.contains(defaultIconBgClass)).toBeTruthy();
});

it('should show expense icon background color when transaction is an expense', () => {
  const results = render(TransactionRow, { transaction: { ...mockTransaction, amount: -98 } });
  const iconElements = results.container.getElementsByTagName('i');

  expect(iconElements.length).toEqual(1);
  expect(iconElements[0].classList.contains(expenseIconBgClass)).toBeTruthy();
  expect(iconElements[0].classList.contains(incomeIconBgClass)).toBeFalsy();
  expect(iconElements[0].classList.contains(defaultIconBgClass)).toBeFalsy();
});

it('should show income icon background color when transaction is an income', () => {
  const results = render(TransactionRow, { transaction: mockTransaction });
  const iconElements = results.container.getElementsByTagName('i');

  expect(iconElements.length).toEqual(1);
  expect(iconElements[0].classList.contains(expenseIconBgClass)).toBeFalsy();
  expect(iconElements[0].classList.contains(incomeIconBgClass)).toBeTruthy();
  expect(iconElements[0].classList.contains(defaultIconBgClass)).toBeFalsy();
});

it('should show expense text color for amount when transaction amount < 0', () => {
  const results = render(TransactionRow, { transaction: mockTransaction });
  const amountElement = results.getByText('$98.00');

  expect(() => amountElement).not.toThrow();
  expect(amountElement.classList.contains(expenseTextClass)).toBeFalsy();
  expect(amountElement.classList.contains(incomeTextClass)).toBeTruthy();
  expect(amountElement.classList.contains(defaultTextClass)).toBeFalsy();
});

it('should show income text color for amount when transaction amount > 0', () => {
  const results = render(TransactionRow, { transaction: { ...mockTransaction, amount: -98 } });
  const amountElement = results.getByText('-$98.00');

  expect(() => amountElement).not.toThrow();
  expect(amountElement.classList.contains(expenseTextClass)).toBeTruthy();
  expect(amountElement.classList.contains(incomeTextClass)).toBeFalsy();
  expect(amountElement.classList.contains(defaultTextClass)).toBeFalsy();
});
