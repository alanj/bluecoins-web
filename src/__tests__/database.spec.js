import {
  getTransactions,
  validateDatabaseFile,
} from '@ts/database';

describe('validateDatabaseFile', () => {
  test('when database file is valid', () => {
    const mockFile = { name: 'TEST.fydb' };
    expect(validateDatabaseFile(mockFile)).toBeTruthy();
  });

  test('when database file is invalid', () => {
    const mockFile = { name: 'TEST.txt' };
    expect(validateDatabaseFile(mockFile)).toBeFalsy();
  });
});

describe('getTransactions', () => {
  test('returns valid transactions', () => {
    const mockStep = jest.fn();
    const mockGetAsObject = jest.fn();
    const mockStatement = {
      step: mockStep,
      getAsObject: mockGetAsObject,
    };
    const mockDb = { prepare: jest.fn() };
    mockDb.prepare.mockReturnValue(mockStatement);
    mockStep
      .mockReturnValueOnce(true)
      .mockReturnValueOnce(true)
      .mockReturnValueOnce(true)
      .mockReturnValue(false);
    mockGetAsObject
      .mockReturnValueOnce(3)
      .mockReturnValueOnce(2)
      .mockReturnValueOnce(7)
      .mockReturnValue(10);

    const actualTransactions = getTransactions(mockDb);

    expect(actualTransactions).toEqual([3, 2, 7]);
  });
});
