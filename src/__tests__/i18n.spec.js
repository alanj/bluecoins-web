import { formatCurrency } from '@ts/i18n';

describe('formatCurrency', () => {
  test('when browser locale is empty', () => {
    expect(formatCurrency('USD', 1234)).toEqual('$1,234.00');
  });

  test('when amount is negative', () => {
    expect(formatCurrency('USD', -1234)).toEqual('-$1,234.00');
  });

  test('when currency is EUR', () => {
    expect(formatCurrency('EUR', -1234)).toEqual('-€1,234.00');
  });

  // TODO: need to add tests for nav.language case & long amount
});
