import initSqlJs, { Database, SqlJsConfig, Statement } from 'sql.js';

export type Transaction = {
  itemName: string;
  accountBalance: number;
  accountName: string;
  amount: number;
  transactionTypeName: string;
  transactionCurrency: string;
  childCategoryName: string;
  parentCategoryName: string;
  labelName: string;
  notes: string;
  time: string;
  date: string;
};

// eslint-disable-next-line no-shadow
enum TransactionType {
  EXPENSE,
  INCOME,
  TRANSFER,
  NEW_ACCOUNT
}

const query = `
  SELECT
  strftime('%d/%m', T.date) as date,
  strftime('%H:%M', T.date) as time,
  A.accountName,
  TT.transactionTypeName,
  I.itemName,
  T.transactionCurrency,
  T.amount / 1000000 AS amount,
  PC.parentCategoryName,
  CC.childCategoryName,
  T.notes, L.labelName,
  (SELECT SUM(TIN.amount)/1000000 FROM TRANSACTIONSTABLE TIN WHERE T.accountId = TIN.accountID AND T.date >= TIN.date AND TIN.reminderGroupID IS NULL) AS accountBalance
FROM TRANSACTIONSTABLE T
INNER JOIN ITEMTABLE I ON I.itemTableID = T.itemID
INNER JOIN ACCOUNTSTABLE A ON A.accountsTableID = T.accountID
INNER JOIN TRANSACTIONTYPETABLE TT ON TT.transactionTypeTableID = T.transactionTypeID
INNER JOIN CHILDCATEGORYTABLE CC ON CC.categoryTableID = T.categoryID
INNER JOIN PARENTCATEGORYTABLE PC ON PC.parentCategoryTableID = CC.parentCategoryID
LEFT OUTER JOIN LABELSTABLE L ON L.transactionIDLabels = T.transactionsTableID
WHERE T.reminderGroupID IS NULL AND T.amount != 0
ORDER BY T.date DESC`;

type CreateDatabaseFromFileReader = (fileReader: FileReader) => Promise<Database> ;
const createDatabaseFromFileReader: CreateDatabaseFromFileReader = (fileReader) => {
  const fileContents = new Uint8Array(fileReader.result as ArrayBuffer);
  const sqlJsConfig: SqlJsConfig = {
    locateFile: (file) => `${file}`,
  };

  return initSqlJs(sqlJsConfig).then((SQL) => new SQL.Database(fileContents));
};

type GetTransactions = (db: Database) => Transaction[];
const getTransactions: GetTransactions = (db) => {
  const stmt: Statement = db.prepare(query);
  const transactions = [];
  while (stmt.step()) {
    const transactionRow = stmt.getAsObject() as unknown;
    transactions.push(transactionRow as Transaction);
  }

  return transactions;
};

type ValidateDatabaseFile = (file: File) => boolean;
const validateDatabaseFile: ValidateDatabaseFile = (file) => {
  const fileNameSplit = file?.name?.split('.');
  return fileNameSplit?.length && fileNameSplit[fileNameSplit.length - 1] === 'fydb';
};

export {
  createDatabaseFromFileReader,
  getTransactions,
  validateDatabaseFile,
  TransactionType,
};
