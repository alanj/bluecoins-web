type GetLocaleFromBrowser = () => string;
const getLocaleFromBrowser: GetLocaleFromBrowser = () => {
  const { navigator } = window;
  if (navigator.languages && navigator.languages.length) {
    return navigator.languages[0];
  }

  return (navigator as any).userLanguage || navigator.language || (navigator as any).browserLanguage || 'en-US';
};

type GetFormat = (currency: string) => Intl.NumberFormat;
const getFormat: GetFormat = (currency) => new Intl.NumberFormat(getLocaleFromBrowser(), { style: 'currency', currency });

type FormatCurrency = (currency: string, amount: number) => string;
const formatCurrency: FormatCurrency = (currency, amount) => getFormat(currency).format(amount);

// eslint-disable-next-line import/prefer-default-export
export { formatCurrency };
