module.exports = {
  mode: 'jit',
  purge: ['./dist/index.html', './src/**/*.svelte'],
  darkMode: false,
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
