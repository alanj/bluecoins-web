const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SveltePreprocess = require('svelte-preprocess');

module.exports = {
  constants: {
    path,
    CopyPlugin,
    HtmlWebpackPlugin,
    MiniCssExtractPlugin,
    appTitle: 'Bluecoins Web',
  },
  config: {
    entry: {
      index: './src/index.js',
    },
    resolve: {
      fallback: {
        fs: false,
        crypto: false,
      },
      // see below for an explanation
      alias: {
        svelte: path.resolve('node_modules', 'svelte'),
        '@ts': path.resolve(__dirname, 'src', 'ts'),
        '@svelte': path.resolve(__dirname, 'src', 'svelte'),
      },
      extensions: ['.tsx', '.ts', '.mjs', '.js', '.svelte'],
      mainFields: ['svelte', 'browser', 'module', 'main'],
    },
    module: {
      rules: [
        {
          test: /\.(html|svelte)$/,
          use: {
            loader: 'svelte-loader',
            options: {
              preprocess: SveltePreprocess({}),
            },
          },
        },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            // https://webpack.js.org/loaders/css-loader/#importloaders
            { loader: 'css-loader', options: { importLoaders: 1 } },
            'postcss-loader',
          ],
        },
        {
          // required to prevent errors from Svelte on Webpack 5+, omit on Webpack 4
          test: /node_modules\/svelte\/.*\.mjs$/,
          resolve: {
            fullySpecified: false,
          },
        },
        {
          test: /\.wasm$/,
          type: 'javascript/auto',
        },
      ],
    },
  },
};
