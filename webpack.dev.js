const { merge } = require('webpack-merge');
const common = require('./webpack.common');

const { constants } = common;
const {
  CopyPlugin, HtmlWebpackPlugin, MiniCssExtractPlugin, path,
} = constants;
const mode = 'development';

module.exports = merge(common.config, {
  mode,
  devtool: 'inline-source-map',
  devServer: {
    static: './dist',
  },
  resolve: {
    alias: {
      'sql.js': path.resolve(__dirname, './node_modules/sql.js/dist/sql-wasm-debug.js'),
    },
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'node_modules/sql.js/dist/sql-wasm-debug.wasm'),
          to: path.resolve(__dirname, 'dist/sql-wasm-debug.wasm'),
        },
      ],
    }),
    new HtmlWebpackPlugin({
      title: constants.appTitle,
    }),
    // required to extract PostCSS(tailwindcss) output from inline to a separate CSS file
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
});
