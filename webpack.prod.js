const { merge } = require('webpack-merge');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const common = require('./webpack.common');

const { constants } = common;
const {
  CopyPlugin, HtmlWebpackPlugin, MiniCssExtractPlugin, path,
} = constants;
const mode = 'production';

module.exports = merge(common.config, {
  mode,
  resolve: {
    alias: {
      'sql.js': path.resolve(__dirname, './node_modules/sql.js/dist/sql-wasm.js'),
    },
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'node_modules/sql.js/dist/sql-wasm.wasm'),
          to: path.resolve(__dirname, 'dist/sql-wasm.wasm'),
        },
      ],
    }),
    new HtmlWebpackPlugin({
      title: constants.appTitle,
    }),
    // required to extract PostCSS(tailwindcss) output from inline to a separate CSS file
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
  ],
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  optimization: {
    minimizer: [
      // Minify CSS
      new CssMinimizerPlugin(),
      // Minify JS
      new UglifyJsPlugin({ parallel: true }),
    ],
  },
});
